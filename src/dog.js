class Dog {
  bark() {
    return 'Bark';
  }

  jump() {
    return 'Jump';
  }

  eat() {
    return 'Eat';
  }
}

module.exports = { Dog }
